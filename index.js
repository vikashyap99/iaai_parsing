// Parses Damage Images from https://www.iaai.com/

const puppeteer = require('puppeteer')
const scrollPageToBottom = require('puppeteer-autoscroll-down')
const fs = require('fs');
const https = require('https');
const axios = require('axios')

const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
  })


const url = 'https://iaai.com/Search?url=0uJuO4swk5yOZ50E98xztOfU%2f88LQVF8TYLpM9%2bIlRo%3d'
const baadalUrl = "https://baadal-dev.roadzen.xyz/api/v1/files/signedUrls";


async function Scrap(url){

    let n
    readline.question('Nubmer of images required?\n', input => {
        n = input
        readline.close();
      });

      
    const browser = await puppeteer.launch()
    const page = await browser.newPage()
    await page.goto(url)
    await scrollPageToBottom(page)
    
    let urls = []
    let images = []
    let x = 0, limit = n;
    
    await page.click('body > div.introjs-tooltipReferenceLayer > div > div.introjs-tooltipbuttons > a.introjs-button.introjs-skipbutton')
    await page.waitFor(4000)
    
    let flag = 0
    while(true){

        if(flag == 1)
            break
                
        
        for(let i = 1; i<=10; i++){
            console.log(x,i)
            if(x == 0)
                await page.click(`#PageNumber${i}`)
            else if(i == 10 )
                await page.click(`#PageNumber${x+1}0`)
            else await page.click(`#PageNumber${x}${i}`)
            await page.waitFor(4000)
            await scrollPageToBottom(page)
            const url = await page.evaluate(() => {
                const tds = Array.from(document.querySelectorAll("#ListingGrid > div.table-body.border-l.border-r > div > div > div.table-cell.table-cell--image.js-intro-Thumbnail > a > img"))
                return tds.map(td => td.src)
            })
            
            urls = urls.concat(...url)
            if(urls.length >= limit){
                flag = 1
                console.log(urls.length, 'urls')
                break
            }

        }

        const endCheck = await page.evaluate(() => {
            const check = document.querySelector('#divPagination > div > div > div > button.btn-last.btn-next-10')
            return check.disabled
        })

        console.log(endCheck,'check')

        await page.click('#divPagination > div > div > div > button.btn-last.btn-next-10')
        await page.waitFor(4000)
        x += 1

    }

    browser.close()

    for (let url of urls) {
        let str = url
        images.push(str.replace("~SID~I1&width=400&height=300", "~SID~B327~S0~I1~RW2592~H1944~TH0&width=1062&height=633"))
    }

    const download = (url, destination) => new Promise((resolve, reject) => {
        const file = fs.createWriteStream(destination,(err, result) => {
            if(err) console.log(err)
            else console.log(result)
        });

        https.get(url, response => {
            response.pipe(file);
            // console.log(response)
            file.on('finish', () => {
                file.close(resolve(true));
            });
        }).on('error', (error) => {
            fs.unlink(destination,(err) => {console.log(err)});
            reject(error.message);

        });
    });
    let count  = 0
    let carProfile = []
    for (let i = 0; i < images.length; i++) {
        count += 1
        let imgCount = 0

        let folder = images[i].slice(39, 47)
        carProfile.push(folder)
        

        if (!fs.existsSync(`./iaai/${folder}`)) {

            fs.mkdirSync(`./iaai/${folder}`, function (err) {
                if (err) {
                    console.log(err)
                } else {
                    console.log("New directory successfully created.")
                }
            })
        } else continue



        for (let j = 0; j <= 10; j++) {

            imgCount += 1
            let url = images[i]
            url = url.replace('I1', ('I' + imgCount.toString()))
            console.log(url)
            result = await download(url, `./iaai/${folder}/${j}.png`)
            .catch((err) => console.log(err));

        }
        if(i == images.length-1){
            console.log(images.length,"image")
            for(let k = 0; k<carProfile.length; k++){
                
                let flag2 = 0
                for(let t = 0; t<10; t++){

                    if(flag2 == 1)
                        break

                    let key = `iaai/${carProfile[k]}/${t}.png`
                    if (!fs.existsSync(key))
                        continue

                    let data = `{
                        "action": "auto",
                        "bucket": "rz-chaepi-dev",
                        "keys": [
                            {
                                "key": "${key}",
                                "resourceId": "test",
                                "expiresIn": 3600
                            }
                        ],
                        "provider": "aws",
                        "region": "ap-south-1",
                        "webhook": "test"
                    }`;

                    
                    let getUrls = downFun(data)
                    getUrls.then( (response) => {
                        let image_path = `iaai/${carProfile[k]}/${t}.png`
                        let res = response.data.urls[0]
                        console.log(res.action)
                        if(res.action === 'get')
                            flag2 = 1
                        else{
                            uplFun(res.signedUrl,image_path)
                        }
                    })
                    .catch((err) => console.log(err))

                }
            }
        }
            

    }
    
    
    
}

Scrap(url)


const downFun = async (data) => {
    console.log(data)
  const down = await axios.post(baadalUrl, data, {
    headers: {
      "rz-cuid": "roadzen-via",
      "Content-Type": "application/json",
    }
  })
  return (down)
}

const uplFun = async (image_signed_url,image_path) => {
    const upl = await axios.put(image_signed_url,
      {data: fs.readFileSync(image_path)},
      {headers: {
        'Content-Type': '',
    }}
      )
    console.log(upl.status)
  }